package br.com.itau.ContatosOAuth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatosOAuth2Application {

	public static void main(String[] args) {
		SpringApplication.run(ContatosOAuth2Application.class, args);
	}

}
