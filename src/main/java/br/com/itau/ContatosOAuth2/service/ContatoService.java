package br.com.itau.ContatosOAuth2.service;

import br.com.itau.ContatosOAuth2.model.Contato;
import br.com.itau.ContatosOAuth2.repository.ContatoRepository;
import br.com.itau.ContatosOAuth2.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato criarContato (Contato contato, @AuthenticationPrincipal Usuario usuario){
        contato.setUsuarioId(usuario.getId());
        Contato contatoObjeto = contatoRepository.save(contato);
        return contatoObjeto;
    }

    public Iterable<Contato> exibirTodosPorUsuario (int id){
        Iterable<Contato> contatos = contatoRepository.findAllByUsuarioId(id);
        return contatos;
    }
}
