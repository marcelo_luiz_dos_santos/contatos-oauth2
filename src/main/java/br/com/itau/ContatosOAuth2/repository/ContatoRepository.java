package br.com.itau.ContatosOAuth2.repository;

import br.com.itau.ContatosOAuth2.model.Contato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository <Contato, Integer> {
    Iterable<Contato> findAllByUsuarioId(int id);
}
