package br.com.itau.ContatosOAuth2.controller;

import br.com.itau.ContatosOAuth2.model.Contato;
import br.com.itau.ContatosOAuth2.security.Usuario;
import br.com.itau.ContatosOAuth2.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping("/contato")
    @ResponseStatus(HttpStatus.CREATED)
    public Contato criarContato (@RequestBody @Valid Contato contato, @AuthenticationPrincipal Usuario usuario){
        Contato contatoObjeto = contatoService.criarContato(contato, usuario);
        return contatoObjeto;
    }

    @GetMapping("/contatos")
    public Iterable<Contato> exibirTodosPorUsuario (@AuthenticationPrincipal Usuario usuario){
        Iterable<Contato> contatos = contatoService.exibirTodosPorUsuario(usuario.getId());
        return contatos;
    }
}
