package br.com.itau.ContatosOAuth2.security;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UsuarioPrincipalExtractor implements PrincipalExtractor {
    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Usuario usuario = new Usuario();

        usuario.setId((Integer) map.get("id"));
        usuario.setNome((String) map.get("nome"));

        return usuario;
    }
}
